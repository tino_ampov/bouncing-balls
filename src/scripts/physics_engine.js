import { Vector } from './vector.js';

// Constructor of the physics engine class
function PhysicsEngine(width, height, gravity) {
  // Properties of the graphic engine
  this.width = width;
  this.height = height;
  this.gravity = gravity;

  // Placeholder for objects registered in the engine
  this.objects = [];
}

PhysicsEngine.prototype = {
  // Register new object
  registerObject: function (object) {
    this.objects.push(object);
  },
  // Unregister object
  unregisterObject: function (object) {
    const index = this.objects.indexOf(object);
    if (index !== -1) {
      this.objects.splice(index, 1);
    }
  },
  // Update objects position
  updatePosition: function () {
    // Calculate time from last update
    const now = Date.now();
    const timePassed = (now - this.lastUpdate) / 1000;

    // Calculate position
    this.objects.forEach(object => {
      this.calculatePosition(object.mass, object.velocity, object.position, timePassed);
      if (object.isColliding(this.height)) {
        this.collisionResolver(object.position,
          object.velocity,
          object.radius,
          object.elasticity);
      }
    });
    this.lastUpdate = now;
  },
  // Calculate object position
  calculatePosition: function (mass, velocity, position, time) {
    var acceleration = Vector.multiply(this.gravity, mass);
    var newVelocity = Vector.multiply(acceleration, time);
    velocity.add(newVelocity);
    var newPosition = Vector.multiply(velocity, time);
    position.add(newPosition);
  },
  // Calculate position after collision
  collisionResolver: function (position, velocity, radius, elasticity) {
    position.y = this.height - radius;
    velocity.y *= -elasticity;
  }
};

export { PhysicsEngine };