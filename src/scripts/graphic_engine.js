// Constructor of the graphic engine class
function GraphicEngine(pixelRatio, context, width, height) {
    // Properties of the graphic engine
    this.pixelRatio = pixelRatio;
    this.context = context;
    this.width = width;
    this.height = height;

    // Placeholder for objects registered in the engine
    this.objects = [];
}

GraphicEngine.prototype = {
    // Register new object
    registerObject: function (object) {
        this.objects.push(object);
    },
    // Unregister object
    unregisterObject: function (object) {
        const index = this.objects.indexOf(object);
        if (index !== -1) {
            this.objects.splice(index, 1);
        }
    },
    draw: function () {
        // Clear canvas
        this.context.clearRect(0, 0, this.width, this.height);
        // Draw objects
        this.objects.forEach(object => {
            object.draw(this.context, this.pixelRatio);
        });
    },
};

export { GraphicEngine };
