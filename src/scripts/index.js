import '../styles/index.scss';
import { Vector } from './vector.js';
import { PhysicsEngine } from './physics_engine';
import { GraphicEngine } from './graphic_engine';
import { Circle } from './circle';

window.onload = function () {
    // Variables for environment details
    var pixelRatio = 100;
    var width = 500;
    var height = 500;
    var physicalWidth = width / pixelRatio;
    var physicalHeight = height / pixelRatio;
    var gravity = new Vector(0, 9.8);
    var minSpeed = 1;
    var maxSpeed = 3;
    var minAngle = 0;
    var maxAngle = 360;

    var elasticity = 0.7;
    var mass = 1;
    var radius = 5;
    var color = 'blue';

    // Set canvas width and height
    var canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    var context = canvas.getContext("2d");

    // Initialize the engines
    var physicsEngine = new PhysicsEngine(physicalWidth, physicalHeight, gravity);
    var graphicEngine = new GraphicEngine(pixelRatio, context, width, height);

    // Click event for drawing the circles
    canvas.addEventListener("click", function (event) {
        // Adjust mouse position 
        var rect = this.getBoundingClientRect();
        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;

        // Calculate for physical world
        var physicalX = x / pixelRatio;
        var physicalY = y / pixelRatio;
        var physicalRadius = radius / pixelRatio;

        // Create circle with random speed and angle
        Circle.randomCircle(physicalX, physicalY, minSpeed, maxSpeed, minAngle, maxAngle, mass, elasticity, physicalRadius, color, physicsEngine, graphicEngine);
    });

    // Main function for drawing objects
    (function update() {
        // Draw objects
        graphicEngine.draw();

        // Update objects position
        physicsEngine.updatePosition();

        requestAnimationFrame(update);
    })();

    /* ENVIRONMENT DETAILS FUNCTIONS */

    // Form submit function
    var form = document.getElementById("form");
    form.onsubmit = function (e) {
        newEnvironment();
        e.preventDefault();
    };

    // Setting new environment from form info
    function newEnvironment() {
        width = parseFloat(document.getElementById("width").value);
        height = parseFloat(document.getElementById("height").value);
        gravity.set(0, parseFloat(document.getElementById("gravity").value));
        elasticity = parseFloat(document.getElementById("elasticity").value);
        mass = parseFloat(document.getElementById("mass").value);
        radius = parseFloat(document.getElementById("radius").value);
        minSpeed = parseFloat(document.getElementById("minSpeed").value);
        maxSpeed = parseFloat(document.getElementById("maxSpeed").value);
        minAngle = parseFloat(document.getElementById("minAngle").value);
        maxAngle = parseFloat(document.getElementById("maxAngle").value);

        physicalWidth = width / pixelRatio;
        physicalHeight = height / pixelRatio;
        canvas.width = width;
        canvas.height = height;
        context = canvas.getContext("2d");
        physicsEngine = new PhysicsEngine(physicalWidth, physicalHeight, gravity);
        graphicEngine = new GraphicEngine(pixelRatio, context, width, height);
    }
};
