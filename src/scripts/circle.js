import { Vector } from './vector.js';
import { getRandomFloat } from './utils.js';

// Constructor of the circle class
function Circle(x, y, speed, angle, mass, elasticity, radius, color, physicsEngine, graphicEngine) {
    // Properties of the circle entity
    this.position = new Vector(x, y);
    this.velocity = new Vector(
        speed * Math.cos(angle - Math.PI / 2),
        speed * Math.sin(angle - Math.PI / 2)
    );
    this.mass = mass;
    this.elasticity = elasticity;
    this.radius = radius;
    this.color = color;

    // Register the circle in the engines
    physicsEngine.registerObject(this);
    graphicEngine.registerObject(this);
}

/* INSTANCE METHODS */
Circle.prototype = {
    // Drawing the circle entity
    draw: function (context, pixelRatio) {
        context.beginPath();
        context.arc(
            this.position.x * pixelRatio,
            this.position.y * pixelRatio,
            this.radius * pixelRatio,
            0,
            Math.PI * 2
        );
        context.fillStyle = this.color;
        context.fill();
        context.closePath();
    },
    // Checking for collision with the floor
    isColliding: function (height) {
        return this.position.y + this.radius > height;
    }
};

/* STATIC METHODS */

// Create circle with random speed and angle
Circle.randomCircle = function (x, y, speedMin, speedMax, angleMin, angleMax, mass, elasticity, radius, color, physicsEngine, graphicEngine) {
    var speed = getRandomFloat(speedMin, speedMax);
    var angle = getRandomFloat(angleMin, angleMax);
    return {
        speed: speed,
        angle: angle,
        circle: new Circle(x, y, speed, angle, mass, elasticity, radius, color, physicsEngine, graphicEngine)
    };
};

export { Circle };