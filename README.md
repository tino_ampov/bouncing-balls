# Bouncing Balls

## Installing dependencies

```
npm install
```

## Starting dev server

```
npm start
```

## Running tests

```
npm run test
```

## Building production version

```
npm run build
```

## Webpack starter used
https://github.com/wbkd/webpack-starter
## Test library used
https://jestjs.io/

## Notes

For simplicity the collision detection and resolving works only with the y-axis (floor). In a real physics simulation this calculation will be much more complex