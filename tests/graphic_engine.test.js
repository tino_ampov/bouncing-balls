import { GraphicEngine } from '../src/scripts/graphic_engine';

const pixelRatio = 100;
const width = 500;
const height = 500;

const ctx = new CanvasRenderingContext2D({ width: width, height: height });
const graphicEngine = new GraphicEngine(pixelRatio, ctx, width, height);
const registerObject = {
    a: 1
}

test('Register object', () => {
    graphicEngine.registerObject(registerObject);
    expect(graphicEngine.objects.length).toBeGreaterThan(0);
});

test('Unregister object', () => {
    graphicEngine.unregisterObject(registerObject);
    expect(graphicEngine.objects.length).toEqual(0);
});