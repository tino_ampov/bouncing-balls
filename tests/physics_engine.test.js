import { Vector } from '../src/scripts/vector';
import { PhysicsEngine } from '../src/scripts/physics_engine';

const pixelRatio = 100;
const width = 500;
const height = 500;
const physicalWidth = width / pixelRatio;
const physicalHeight = height / pixelRatio;
const gravity = new Vector(0, 9.8);

const physicsEngine = new PhysicsEngine(physicalWidth, physicalHeight, gravity);
const registerObject = {
    a: 1
}

test('Register object', () => {
    physicsEngine.registerObject(registerObject);
    expect(physicsEngine.objects.length).toBeGreaterThan(0);
});

test('Unregister object', () => {
    physicsEngine.unregisterObject(registerObject);
    expect(physicsEngine.objects.length).toEqual(0);
});

test('Calculate position test', () => {
    const mass = 1;
    const velocity = new Vector(1, 1);
    const position = new Vector(1, 1);
    const time = 1;

    physicsEngine.calculatePosition(mass, velocity, position, time);
    expect(position).toEqual({ x: 2, y: 11.8 })
});

test('Calculate position after collision', () => {
    const velocity = new Vector(1, 1);
    const position = new Vector(1, 1);
    const radius = 1;
    const elasticity = 0.5;


    physicsEngine.collisionResolver(position, velocity, radius, elasticity);
    expect(position).toEqual({ x: 1, y: 4 })
    expect(velocity).toEqual({ x: 1, y: -0.5 })
});