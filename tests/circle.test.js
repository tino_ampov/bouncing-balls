import { Vector } from '../src/scripts/vector';
import { PhysicsEngine } from '../src/scripts/physics_engine';
import { GraphicEngine } from '../src/scripts/graphic_engine';
import { Circle } from '../src/scripts/circle';

const pixelRatio = 100;
const width = 500;
const height = 500;
const physicalWidth = width / pixelRatio;
const physicalHeight = height / pixelRatio;
const gravity = new Vector(0, 9.8);

const ctx = new CanvasRenderingContext2D({ width: width, height: height });
const physicsEngine = new PhysicsEngine(physicalWidth, physicalHeight, gravity);
const graphicEngine = new GraphicEngine(pixelRatio, ctx, width, height);

test('Create circle with all parameters', () => {
    const speed = 1;
    const angle = 1;
    const physicalX = 5;
    const physicalY = 5;
    const mass = 2;
    const elasticity = 0.5;
    const physicalRadius = 0.2;
    const color = "red";
    const circle = new Circle(physicalX, physicalY, speed, angle, mass, elasticity, physicalRadius, color, physicsEngine, graphicEngine);

    expect(circle.position).toEqual({ x: 5, y: 5 })
    expect(circle.mass).toEqual(mass)
    expect(circle.elasticity).toEqual(elasticity)
    expect(circle.radius).toEqual(physicalRadius)
    expect(circle.color).toEqual(color)
    expect(circle.velocity).toEqual({ x: 0.8414709848078965, y: -0.5403023058681397 })

    expect(physicsEngine.objects.length).toBeGreaterThan(0);
    expect(graphicEngine.objects.length).toBeGreaterThan(0);

    physicsEngine.unregisterObject(circle);
    graphicEngine.unregisterObject(circle);
    expect(physicsEngine.objects.length).toEqual(0);
    expect(graphicEngine.objects.length).toEqual(0);
});

test('Create circle with random speed and angle', () => {
    const physicalX = 5;
    const physicalY = 5;
    const mass = 2;
    const elasticity = 0.5;
    const physicalRadius = 0.2;
    const color = "red";
    const minSpeed = 1;
    const maxSpeed = 2;
    const minAngle = 0;
    const maxAngle = 180;
    const result = Circle.randomCircle(physicalX, physicalY, minSpeed, maxSpeed, minAngle, maxAngle, mass, elasticity, physicalRadius, color, physicsEngine, graphicEngine);
    expect(result.circle.position).toEqual({ x: 5, y: 5 })
    expect(result.circle.mass).toEqual(mass)
    expect(result.circle.elasticity).toEqual(elasticity)
    expect(result.circle.radius).toEqual(physicalRadius)
    expect(result.circle.color).toEqual(color)
    expect(result.speed).toBeGreaterThanOrEqual(minSpeed)
    expect(result.speed).toBeLessThanOrEqual(maxSpeed)
    expect(result.angle).toBeGreaterThanOrEqual(minAngle)
    expect(result.angle).toBeLessThanOrEqual(maxAngle)

    expect(physicsEngine.objects.length).toBeGreaterThan(0);
    expect(graphicEngine.objects.length).toBeGreaterThan(0);

    physicsEngine.unregisterObject(result.circle);
    graphicEngine.unregisterObject(result.circle);
    expect(physicsEngine.objects.length).toEqual(0);
    expect(graphicEngine.objects.length).toEqual(0);
});

test('Circle collision check', () => {
    const physicalX = 5;
    const physicalY = 4.8;
    const mass = 2;
    const elasticity = 0.5;
    const physicalRadius = 0.3;
    const color = "red";
    const minSpeed = 1;
    const maxSpeed = 2;
    const minAngle = 0;
    const maxAngle = 180;

    const result = Circle.randomCircle(physicalX, physicalY, minSpeed, maxSpeed, minAngle, maxAngle, mass, elasticity, physicalRadius, color, physicsEngine, graphicEngine);
    expect(result.circle.isColliding(physicalHeight)).toBeTruthy()
});